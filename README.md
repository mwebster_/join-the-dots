# JOIN THE DOTS


[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---
![join.jpg](https://bitbucket.org/repo/p8pjy5e/images/408638539-join.jpg)

## Introduction

A messy mix of techniques to automate join the dot diagrams. Coded in Processing and used as part of an [installation at the Centre Pompidou](https://area03.bitbucket.io/sam) and in relation to a much bigger project called [SAM.](https://samdraws.bitbucket.io/) There are things to imagine with this program. If anyone out there is interested in collaborating on a shared effort, I'm all eyes and ears. Otherwise, this repository may be slow to commits and further developments. 

## Contents

* Sketches >
* Canny edge detection
* Edge detection
* Join The Dots prototype_01
* PolySimplify
* TSP (Travelling Salesman) 


## Install

The program in this repository can be compiled using the Processing environment. It also depends on two libraries, ControlP5 and Blob Detection which need to be installed independantly. 

[https://processing.org/](https://processing.org/)

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.05
* Tools used : Processing, ControlP5 & Blob Detection

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution. You can equally contact me directly via email. 

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
