/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
Canny PCANNY;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  background(0);
  String imgName = "aiFace.jpg"; 
  // @param image name, blur value (if 0, no blur), isGrayScale
  PCANNY = new Canny(imgName, 0, false);
  
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);  
  float t = map(mouseX, 1, width, 0, 255);
  PCANNY.detectEdge(t);

}


/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() { 
  if (key == 'e') {
    saveFrame("CannyEdge_###.png");
  }
}