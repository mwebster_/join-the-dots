/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


/*
 Each pixel has 8 neighbours starting at center and going clockwise out.
 .......
 |9|2|3|
 |8|1|4|
 |7|6|5|
 .......
 */

// These arrays define the pixel offset for x & y
int [] xd = {
  0, 1, 1, 1, 0, -1, -1, -1, 0
};

int [] yd = {
  1, 1, 0, -1, -1, -1, 0, 1, 1
};

int [][] MyCopy;
PImage myImg;
float imgRez = 8;
String imgName = "aiFace.jpg";

/////////////////////////// SETUP ////////////////////////////
void settings() {
  initImage(imgName, 1, false);
}
void setup() {
}

void draw() {
  background(255);
  float t = map(mouseX, 1, width, 0, 255);
  detectEdge(t);
}



void detectEdge(float _bThresh) {
  image(myImg, 0, 0); //display the image
  for (int x=1; x<width-1; x++) //for all pixels (except border)
    for (int y=1; y<height-1; y++) {
      int b=0; // will count dark neighbouring pixels
      int a=0; // will count the consecutive dark pixels
      // _bThresh was originally a constant = 128
      for (int i=0; i<8; i++) {
        if (brightness(get(x+xd[i], y+yd[i]))<_bThresh) //case 1
          b++;
        if (brightness(get(x+xd[i], y+yd[i]))<_bThresh &&
          brightness(get(x+xd[i+1], y+yd[i+1]))>_bThresh) //case 2
          a++;
      }

      // try some different values here
      if ((b>=4 && b<=5) || a==2 )
        //if ((b>=2 && b<=6) || a==1 )// original
        MyCopy[x][y]=1; //mark these ones as edges
      else
        MyCopy[x][y]=0;
    }

  for (int x=1; x<width-1; x++) //go through all pixels
    for (int y=1; y<height-1; y++) {
      if (MyCopy[x][y]==1) //if they are marked
        set(x, y, color(0, 0, 0)); //paint them black
      else
        set(x, y, color(255, 255, 255)); //else white
    }
}

/*
 * Method for loading image & applying blur/grayscale
 * @param : name of image (String)
 * @param : blur value. If 0, no blur is applied (int)
 * @param : apply or not grey scale (boolean)
 */
void initImage(String _imgName, int _blurVal, boolean _grey) {
  myImg = loadImage(_imgName); 
  float screenW = myImg.width*imgRez;
  float screenH = myImg.height*imgRez;
  size((int)screenW, (int)screenH); //size to match the image

  myImg.resize(int(myImg.width*imgRez), int(myImg.height*imgRez));
  if (_grey) {
    myImg.filter(GRAY);
  }
  if (_blurVal>0) {
    fastblur(myImg, _blurVal);
  }
  MyCopy = new int[width][height]; // array equal to image
}


// Super Fast Blur v1.1
// by Mario Klingemann 
// <http://incubator.quasimondo.com>
void fastblur(PImage img, int radius) {
  if (radius<1) {
    return;
  }
  int w=img.width;
  int h=img.height;
  int wm=w-1;
  int hm=h-1;
  int wh=w*h;
  int div=radius+radius+1;
  int r[]=new int[wh];
  int g[]=new int[wh];
  int b[]=new int[wh];
  int rsum, gsum, bsum, x, y, i, p, p1, p2, yp, yi, yw;
  int vmin[] = new int[max(w, h)];
  int vmax[] = new int[max(w, h)];
  int[] pix=img.pixels;
  int dv[]=new int[256*div];
  for (i=0; i<256*div; i++) {
    dv[i]=(i/div);
  }

  yw=yi=0;

  for (y=0; y<h; y++) {
    rsum=gsum=bsum=0;
    for (i=-radius; i<=radius; i++) {
      p=pix[yi+min(wm, max(i, 0))];
      rsum+=(p & 0xff0000)>>16;
      gsum+=(p & 0x00ff00)>>8;
      bsum+= p & 0x0000ff;
    }
    for (x=0; x<w; x++) {

      r[yi]=dv[rsum];
      g[yi]=dv[gsum];
      b[yi]=dv[bsum];

      if (y==0) {
        vmin[x]=min(x+radius+1, wm);
        vmax[x]=max(x-radius, 0);
      }
      p1=pix[yw+vmin[x]];
      p2=pix[yw+vmax[x]];

      rsum+=((p1 & 0xff0000)-(p2 & 0xff0000))>>16;
      gsum+=((p1 & 0x00ff00)-(p2 & 0x00ff00))>>8;
      bsum+= (p1 & 0x0000ff)-(p2 & 0x0000ff);
      yi++;
    }
    yw+=w;
  }

  for (x=0; x<w; x++) {
    rsum=gsum=bsum=0;
    yp=-radius*w;
    for (i=-radius; i<=radius; i++) {
      yi=max(0, yp)+x;
      rsum+=r[yi];
      gsum+=g[yi];
      bsum+=b[yi];
      yp+=w;
    }
    yi=x;
    for (y=0; y<h; y++) {
      pix[yi]=0xff000000 | (dv[rsum]<<16) | (dv[gsum]<<8) | dv[bsum];
      if (x==0) {
        vmin[y]=min(y+radius+1, hm)*w;
        vmax[y]=max(y-radius, 0)*w;
      }
      p1=x+vmin[y];
      p2=x+vmax[y];

      rsum+=r[p1]-r[p2];
      gsum+=g[p1]-g[p2];
      bsum+=b[p1]-b[p2];

      yi+=w;
    }
  }
}