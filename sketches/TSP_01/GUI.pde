int GRIDREZ;
int BLUR_VAL;
int F_VAL;
float THRESH_MIN, THRESH_MAX;
float GAMMA;
float PNTS_SIZE, STROKE;
boolean SHOW_POINTS, SHOW_LINES, SHOW_IMG;
boolean FILTERS, IS_GAMMA;
Bang b1, b2;
DropdownList d1;
String[] filters = {
  "NONE", "POST", "THRESH"
};
String filter;

void interfaces() {
  GUI = new ControlP5(this);
  Slider s1 = GUI.addSlider("THRESH_MIN").
    setSize(50, 10).
    setPosition(150, height-100).setRange(1, 255).setValue(100);

  Slider s1b = GUI.addSlider("THRESH_MAX").
    setSize(50, 10).
    setPosition(150, height-90).setRange(1, 255).setValue(163);


  Slider s2 = GUI.addSlider("GRIDREZ").
    setSize(50, 10).
    setPosition(150, height-80).setRange(1, 10).setValue(1);


  Slider s3 = GUI.addSlider("PNTS_SIZE").
    setSize(50, 10).
    setPosition(20, height-80).setRange(0.1, 20.0).setValue(2);


  Slider s4 = GUI.addSlider("STROKE").
    setSize(50, 10).
    setPosition(20, height-100).setRange(0.1, 20.0).setValue(1.5);


  Toggle t1 = GUI.addToggle("SHOW_LINES")
    .setValue(true)
    .setPosition(20, height-50)
    .setSize(15, 15);

  Toggle t2 = GUI.addToggle("SHOW_POINTS")
    .setValue(false)
    .setPosition(80, height-50)
    .setSize(15, 15);

  Toggle t3 = GUI.addToggle("SHOW_IMG")
    .setValue(true)
    .setPosition(150, height-50)
    .setSize(15, 15);


  /////////////////////////////////////////////// FILTER SECTION

  Toggle t4 = GUI.addToggle("FILTERS")
    .setValue(false)
    .setPosition(300, height-100)
    .setSize(15, 15)
    .setLabel("FILTERS");

  Toggle t5 = GUI.addToggle("IS_GAMMA")
    .setValue(false)
    .setPosition(440, height-100)
    .setSize(15, 15)
    .setLabel("GAMMA");


  // change the trigger event, by default it is PRESSED.
  /*b1 = GUI.addBang("bang")
   .setPosition(380, height-100)
   .setSize(15, 15)
   .setTriggerEvent(Bang.RELEASE)
   .setLabel("recalculate");
   */
  b2 = GUI.addBang("bang2")
    .setPosition(340, height-100)
    .setSize(15, 15)
    .setTriggerEvent(Bang.RELEASE)
    .setLabel("reload");


  Slider s5 = GUI.addSlider("GAMMA").
    setSize(50, 10).
    setPosition(480, height-100).setRange(0.2, 2.3).setValue(0.93);


  Slider s6 = GUI.addSlider("BLUR_VAL").
    setSize(50, 10).
    setPosition(480, height-80).setRange(0, 12).setValue(2);

  Slider s7 = GUI.addSlider("F_VAL").
    setSize(50, 10).
    setPosition(480, height-40).setRange(0, 15).setValue(2);



  /////////////////////////////////////////////
  // drop down list
  d1 = GUI.addDropdownList("FilterType")
    .setPosition(300, height-55)
    .open()
    ;

  // Menu items

  for (int i=0; i<filters.length; i++) {
    d1.addItem(filters[i], i);
  }
}

public void bang() {
  if (FILTERS) {
    applyFilters();
  }
}

public void applyFilters() {
  // APPLYING FILTERS
  // @param : type | val | gamma val | blur val
  int itemIndex = (int)d1.getValue();
  filter = filters[ itemIndex ]; 
  calculateFilter(filter, F_VAL, GAMMA, IS_GAMMA, BLUR_VAL);
}

/*
public void bang2() {
 IMG = loadImage(CHOSEN_IMG);
 //b1.setLabel("recalculate");
 }
 */

void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  if (theEvent.isGroup()) {
    // check if the Event was triggered from a ControlGroup
    println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
    if (theEvent.isFrom(GUI.getGroup("FilterType"))) {
      if (FILTERS) {
        applyFilters();
      }
    }
  } else if (theEvent.isController()) {
    println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
  }
  if (theEvent.isFrom(GUI.getController("GAMMA")) || 
    (theEvent.isFrom(GUI.getController("FILTERS")) ||
    (theEvent.isFrom(GUI.getController("IS_GAMMA")) ||
    (theEvent.isFrom(GUI.getController("F_VAL")) ||
    (theEvent.isFrom(GUI.getController("BLUR_VAL")) ))))) {
    if (FILTERS) {
      applyFilters();
      //b1.setCaptionLabel("Recalculating...");
    }
  }
}