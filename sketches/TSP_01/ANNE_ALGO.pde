/*
 * OUR DRAWING ALGORITHM BASED ON A SIMPLE NEAREST NEIGHBOUR CALCULATION
 *
 */

//////////////////////////////////////////////////////////////////

/*
 * Draws line based on nearest neighbour in an arrayList of points
 */
void drawImage(ArrayList<PVector> _pnts) {

  if (_pnts.size()>0) {
    PVector tempPosition = new PVector(_pnts.get(0).x, _pnts.get(0).y);

    if (PDFEXPORT) {
      TIME = getTime();
      beginRecord(PDF, "export_"+TIME+"_.pdf");
    }
    for (int i=0; i<_pnts.size ()-1; i++) {

      // Get nearest neighbour as an index
      int nearestIndex = getNearestNeighbour(tempPosition, _pnts);
      if (SHOW_LINES) {
        strokeWeight(STROKE);    
        //stroke(255, 200, 0);
        stroke(200, 0, 255);
        line(tempPosition.x, tempPosition.y, _pnts.get(nearestIndex).x, _pnts.get(nearestIndex).y);
      }
      if (SHOW_POINTS) {
        strokeWeight(PNTS_SIZE);
        stroke(255, 200, 0);
        point(tempPosition.x, tempPosition.y);
      }
      // update temp position 
      tempPosition = new PVector(_pnts.get(nearestIndex).x, _pnts.get(nearestIndex).y);
      _pnts.remove(nearestIndex);
    }

    if (PDFEXPORT) {
      endRecord();
      PDFEXPORT = !PDFEXPORT;
    }
  }
}

// Fill our array according to image brightness
void addPixels(float _tMin, float _tMax) {
  PNTS = new ArrayList<PVector>();

  if (SHOW_IMG) {
    image(IMG, 0, 0);
  }
  IMG.loadPixels();
  for (int y = 0; y <IMG.height; y += GRIDREZ) {
    for (int x = 0; x <IMG.width; x += GRIDREZ) {
      //color c = IMG.get(x, y); 
      //float pixelBright = brightness(c);
      int pos = (y * IMG.width) + x;
      float pixelBright = brightness(IMG.pixels[pos]);
      if ((pixelBright>=_tMin) && (pixelBright<=_tMax)) {
        PVector point = new PVector(x*IMG_SCALE, y*IMG_SCALE);        
        PNTS.add( point );
      }
    }
  }
}

int getNearestNeighbour(PVector _currentPnt, ArrayList _pnts) {
  float bestDistance = 50000; 
  int bestIndex=0;
  for (int i = 0; i<_pnts.size (); ++i) {
    PVector testPos = (PVector) _pnts.get(i);
    float d2 = PVector.dist(_currentPnt, testPos);
    if (d2 < bestDistance) {
      bestDistance = d2;
      bestIndex = i;
    }
  }
  return bestIndex;
}