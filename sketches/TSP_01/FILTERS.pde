/*
 * Just some filters for image optimazation
 * NOTE : Turn filters on in GUI to apply filters
 * Press 'reload' button to reload the original image
 *
 */


void gammaCorrection(float _gval) {
  // Optional gamma correction for background image.  
  IMG.loadPixels();
  float tempFloat;  
  float GammaValue = _gval;  // Normally in the range 0.25 - 4.0   
  for (int i = 0; i < IMG.pixels.length; i++) {
    tempFloat = brightness(IMG.pixels[i])/255;  
    IMG.pixels[i] = color(floor(255 * pow(tempFloat, GammaValue)));
  } 
  IMG.updatePixels();
}



void filters(String _filter, int _tVal) {
  IMG = loadImage(CHOSEN_IMG);

  IMG.loadPixels();
  if (_filter.equals("POST")) {
    if (_tVal<2) {
      _tVal = 2;
    }
    IMG.filter(POSTERIZE, _tVal);
  } else if (_filter.equals("THRESH")) {
    float t = map(_tVal, 0.0, 15.0, 0.1, 1.0);
    IMG.filter(THRESHOLD, t);
  } else if (_filter.equals("NONE")) {
    IMG.updatePixels();
  }
  IMG.updatePixels();
}

///////////////////////////////////////////// 
void calculateFilter(String _type, int _val, float _gammaVal, boolean _isGamma, int _blurAmm) {

  if (_type.equals("POST")) {
    filters("POST", _val);
  }
  if (_type.equals("THRESH")) {
    filters("THRESH", _val);
  } 
  if (_type.equals("NONE")) {
    filters("NONE", _val);
  }
  if(_isGamma){
  gammaCorrection(_gammaVal);// 0.93
  }
  IMG.filter(BLUR, _blurAmm); // blur 2
}