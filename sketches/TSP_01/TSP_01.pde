/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// LIBRARIES /////////////////////////

import controlP5.*;
import processing.pdf.*;
import java.util.*;
import java.text.*;

/////////////////////////// GLOBALS ////////////////////////////
ControlP5 GUI;
boolean PDFEXPORT = false;
String TIME;
PImage IMG;
String CHOSEN_IMG;
PFont FONT;

ArrayList<PVector> PNTS;
ArrayList<PVector> DEL_PNTS;
int IMG_SCALE = 9;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(640, 720);
  background(33);
  FONT = loadFont("FiraMono-Regular-48.vlw");
  textFont(FONT, 12);  
  CHOSEN_IMG="aiFace.jpg";
  IMG = loadImage(CHOSEN_IMG);
  interfaces();
}

/////////////////////////// DRAW ////////////////////////////

void draw() {
  background(0); 
  addPixels( THRESH_MIN, THRESH_MAX );
  drawImage( PNTS );
}

void keyPressed() {
  if (key == 'e') {
    PDFEXPORT = !PDFEXPORT;
  }
  if (key == '+') {
    THRESH_MAX+=0.5;
    GUI.getController("THRESH_MAX").setValue(THRESH_MAX);
  } 
  if (key == '-') {
    THRESH_MAX-=0.5;
    GUI.getController("THRESH_MAX").setValue(THRESH_MAX);
  }
} 

String getTime() {
  Date dNow = new Date( );
  SimpleDateFormat time = new SimpleDateFormat ("YYMMhhmmss");
  String t = time.format(dNow);
  return t;
}