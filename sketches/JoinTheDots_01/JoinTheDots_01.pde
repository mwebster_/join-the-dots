/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

import blobDetection.*;
import processing.pdf.*;
import java.text.*;
import java.util.*;

PImage visage, visageFilter;
String chosenImg = "aiFace.jpg";
String imgName = "teapot";
int factor = 8; // image size
int levels = 2;
BlobDetection[] theBlobDetection = new BlobDetection[int(levels)];
DotDiagram[] theDiagrams = new DotDiagram[int(levels)];
int numberPoints;
boolean updateDisplayOfPoints = false;

////////////////////////////////////////
void settings() {
  visage = loadImage(chosenImg);
  size(visage.width*factor, visage.height*factor);
}
////////////////////////////////////////
void setup() {
  colorMode(HSB, 360, 100, 100);
  initGUI();
  compute();
}

////////////////////////////////////////
void draw() {
  background(0);
  if (bDrawImage)
    image(visageFilter, 0, 0, width, height);
  if (bExportPDF) {
    beginRecord(PDF, "export_"+chosenImg+"_"+getTime()+"_"+numberPoints+"_"+".pdf");
    colorMode(HSB, 360, 100, 100);
  }

  if (bDrawContours) {
    for (int i=0; i<levels; i++) {
      drawContours(i);
    }
  }

  if (bDrawDiagram) {
    try {
      // get string value from ScrollableList
      int itemIndex = (int)s1.getValue();
      String algorithm = algorithms[ itemIndex ];
      if (theDiagrams != null) {
        for (int i=0; i<theDiagrams.length; i++) {
          theDiagrams[i].compute(numDots, 20, algorithm); // 10 : SIMPLE [best]
          theDiagrams[i].displayDotDiagram(lineThresh, bDrawDiagramLines, bDrawDiagramDotNumbers, i);
        }
        calculateTotalDiaPoints(levels);
      }
    }
    catch(Exception e) {
      //println("...trying to change preset...");
    }
  }

  if (bExportPDF) {
    endRecord();
    bExportPDF = false;
  }

  if (isGUI) {
    cp5.show();
  } else {
    cp5.hide();
  }
  cp5.draw();
}