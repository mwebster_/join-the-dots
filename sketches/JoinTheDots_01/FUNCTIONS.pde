/*
 * A list of functions
 */

////////////////////////////////////////
void calculateTotalDiaPoints(int _levels) {
  levels = _levels;
  numberPoints = 0;
  for (int i=0; i<levels; i++) {
    numberPoints += theDiagrams[i].getNumPoints();
  }
}

void computeBlobs() {
  theBlobDetection = new BlobDetection[(int)levels];
  for (int i=0; i<levels; i++) {
    theBlobDetection[i] = new BlobDetection(visageFilter.width, visageFilter.height);
    theBlobDetection[i].setThreshold(float(i)/float(levels) * levelMax);
    theBlobDetection[i].computeBlobs(visageFilter.pixels);
  }
}

////////////////////////////////////////
void computeDiagrams() {
  //println("-- computeDiagrams()");
  if (bComputeGlobalDiagram) {
    theDiagrams = new DotDiagram[1];
    theDiagrams[0] = new DotDiagram();
    ArrayList<PVector> points = new ArrayList<PVector>();
    for (int i=0; i<levels; i++)
      points.addAll( getPVectorFromBlobDetection( theBlobDetection[i] ) );
    theDiagrams[0].setDiagramPoints( points );
  } else {
    theDiagrams = new DotDiagram[(int)levels];
    for (int i=0; i<levels; i++) {
      //println("-- diagram ["+i+"]");
      theDiagrams[i] = new DotDiagram();
      theDiagrams[i].setDiagramPoints( getPVectorFromBlobDetection( theBlobDetection[i] ) );
    }
  }
}

////////////////////////////////////////
void compute() {
  filterVisage(blur, thresh);
  computeBlobs();
  computeDiagrams();
}

////////////////////////////////////////
void drawContours(int i) {
  Blob b;
  EdgeVertex eA, eB;
  float x, y;
  pushStyle();
  //if (i == 0 || theBlobDetection[i].getBlobNb() == 0) return;

  //if (is_Partial) {
  /////////////////////// draw only some of drawing
  try {
    if (theBlobDetection != null) {
      for (int n=partialFactor; n<theBlobDetection[i].getBlobNb(); n++)
      {
        b=theBlobDetection[i].getBlob(n);

        if (b!=null && (b.w*width)*(b.h*height)>=maxPixelH*maxPixelH)
        {
          stroke((float(i)/float(levels)*colorRange)+colorStart, 100, 100, alphaContours);
          for (int m=partialFactor*partialFine; m<b.getEdgeNb(); m++)
          {
            eA = b.getEdgeVertexA(m);
            eB = b.getEdgeVertexB(m);

            line(eA.x*width, eA.y*height, eB.x*width, eB.y*height);

            if (bDrawContoursVertex)
            {
              rect(eA.x*width, eA.y*height, 4, 4);
            }
          }
        }
      }
      popStyle();
    }
  } 
  catch(Exception e) {
    //println("...just checking...");
  }
}
//////////////// end

/*
  else {
 pushStyle();
 rectMode(CENTER);
 noFill();
 //if(theBlobDetection[i] != null) {
 for (int n=0; n<theBlobDetection[i].getBlobNb(); n++){
 b=theBlobDetection[i].getBlob(n);
 
 if (b!=null && (b.w*width)*(b.h*height)>=maxPixelH*maxPixelH)
 {
 stroke((float(i)/float(levels)*colorRange)+colorStart, 100, 100, alphaContours);
 for (int m=0; m<b.getEdgeNb(); m++)
 {
 eA = b.getEdgeVertexA(m);
 eB = b.getEdgeVertexB(m);
 
 line(eA.x*width, eA.y*height, eB.x*width, eB.y*height);
 
 if (bDrawContoursVertex)
 {
 rect(eA.x*width, eA.y*height, 4, 4);
 }
 }
 }
 }
 popStyle();
 }
 //}
 
 }
 */
////////////////////////////////////////
void filterVisage(int _blur, float _thresh) {
  visageFilter = visage.copy();
  if (_blur>0) {
    visageFilter.filter(BLUR, _blur);
  }

  if (_thresh>0) {
    visageFilter.filter(THRESHOLD, _thresh);
  }
}

////////////////////////////////////////
String getTime() {
  Date dNow = new Date( );
  SimpleDateFormat time = new SimpleDateFormat ("hh"+"_"+"mm"+"_"+"ss");
  println(time.format(dNow));
  String t = time.format(dNow);
  return t;
}

////////////////////////////////////////
void keyPressed() {
  if (key == 'h') {
    isGUI =!isGUI;
  }
  if (key == 's') {
    saveFrame(imgName+".png");
    println("saved a screen capture :–)");
  }
  if (key == 'n') {
    println("Number of actual dots = "+numberPoints);
  }
}

////////////////////////////////////////
ArrayList<PVector> getPVectorFromBlobDetection(BlobDetection bd) {
  ArrayList<PVector> points = new ArrayList<PVector>();
  Blob b;
  EdgeVertex eA, eB;
  for (int n=0; n<bd.getBlobNb(); n++) {
    b=bd.getBlob(n);

    if (b!=null && (b.w*width)*(b.h*height)>=maxPixelH*maxPixelH)
    {
      for (int m=0; m<b.getEdgeNb(); m++)
      {
        eA = b.getEdgeVertexA(m);
        eB = b.getEdgeVertexB(m);
        points.add( new PVector(eA.x*width, eA.y*height) );
      }
    }
  }
  //println("-- found "+points.size()+ " point(s)");
  return points;
}