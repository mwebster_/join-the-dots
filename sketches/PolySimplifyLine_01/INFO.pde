/*
 * :::::::::::::
 * JOIN THE DOTS
 * :::::::::::::
 *
 * Sketch: PolySimplify_01
 * Parent Sketch: none
 * Type: 
 *
 * Summary : I re-wrote Thomas Diewald's simplify method to make things simple.
 *           He implements this algorithm in his blob detection library.
 *           http://thomasdiewald.com
 *
 * GIT: https://bitbucket.org/mwebster_/join-the-dots/src/master/
 * Author: mark webster 2020
 * https://area03.bitbucket.io
 *
 * LICENCE
 * This software is part of a package of pedagogical tools used 
 * with the online website, Computational Graphic Design Manual :
 * https://dpmanual.bitbucket.io
 *
 * Copyright ©2020  mark webster
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/gpl-3.0.html.  
 * 
 */