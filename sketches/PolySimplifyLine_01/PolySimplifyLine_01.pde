/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


ArrayList <PVector> pnts;
ArrayList <PVector> pnts_simple;

void setup() {
  size(700, 300);
  pnts = new ArrayList <PVector>();
  pnts_simple = new ArrayList <PVector>();
  int num = 30;
  float x= 20;
  float y= height/2;
  for (int i=0; i<num; i++) {
    float xx = random(15);
    float yy = random(-40, 40);
    PVector p = new PVector(x+xx, y+yy);
    pnts.add(p);
    x+=22;
  }
}


void draw() {
  background(0);
  noStroke();
  fill(255);
  for (PVector p : pnts) {
    ellipse(p.x, p.y, 15, 15);
  }

  //SIMPLIFY_POLY
  int repeat_simplifying = (int) map(mouseX, 0, width, 0, 50);
  pnts_simple = simplifyPoly(pnts, 10, repeat_simplifying);

  fill(255, 0, 0);
  for (PVector p : pnts_simple) {
    ellipse(p.x, p.y, 10, 10);
  }

  stroke(0, 100, 255);
  strokeWeight(2);
  for (int i=0; i<pnts_simple.size ()-1; i++) {
    PVector p = pnts_simple.get(i);
    PVector p2 = pnts_simple.get(i+1);
    line(p.x, p.y, p2.x, p2.y);
  }

  //info
  noStroke();
  fill(255);
  ellipse(20, height-35, 15, 15);
  text("ORIGINAL", 40, height-30);
  fill(255, 0, 0);
  ellipse(20, height-15, 10, 10);
  fill(200, 0, 0);
  text("SIMPLIFIED", 40, height-10);
}

////////////////////////////////////////////////// FUNCTIONS

void mousePressed() {
  setup();
}

void keyPressed(){}

/**
 *  
 * returns a new simplified list of points, from a given pvector
 * author: thomas diewald
 *
 * @param polyline to simplify
 * @param step    the number of vertices in a row, that are checked for the maximum offset
 * @param max_offset maximum offset a vertice can have 
 * @return new simplified polygon
 */
public static final ArrayList<PVector> simplifyPoly(  ArrayList<PVector> polyline, int step, float max_offset ) {
  ArrayList<PVector> poly_simp = new ArrayList<PVector>();
  //polyline = new ArrayList<PVector>();
  int index_cur  = 0;
  int index_next = 0;
  poly_simp.add(polyline.get(index_cur));

  while ( index_cur != polyline.size ()-1 ) {
    index_next = ((index_cur + step) >= polyline.size()) ? (polyline.size()-1) :  (index_cur + step) ;

    PVector p_cur  = polyline.get(index_cur);
    PVector p_next = polyline.get(index_next);

    while ( (++index_cur < index_next) &&  (max_offset > abs(distancePoint2Line(p_cur, p_next, polyline.get(index_cur))) ) );
    poly_simp.add(polyline.get(index_cur));
  }
  return poly_simp;
}



/**
 * author: thomas diewald
 * returns the shortest distance of a point(p3) to a line (p1-p2)
 */

public static final float distancePoint2Line(PVector p1, PVector p2, PVector p3 ) {
  float x1 = p2.x-p1.x, y1 = p2.y-p1.y;
  if ( x1 == 0 && y1 == 0)
    return 0;
  float x2 = p3.x-p1.x, y2 = p3.y-p1.y;
  if ( x2 == 0 && y2 == 0)
    return 0;
  float A = x1*y2 - x2*y1;
  if ( A == 0)
    return 0;
  float p1p2_mag = (float)Math.sqrt(x1*x1 + y1*y1);
  return A/p1p2_mag;
}